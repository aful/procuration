'use strict'

// Fold all .foldable elems by default
document.querySelectorAll('.foldable').forEach(function(elem) {
    elem.setAttribute('hidden', 'hidden')
})

// Unfold corresponding .foldable elems when mouseenter on .fold-trigger elems
document.querySelectorAll('.fold-trigger').forEach(function(elem) {
    const enclosing_form = elem.form
    elem.addEventListener('click', function(evt) {
        evt.preventDefault()

        enclosing_form.querySelectorAll('.foldable').forEach(function(elem) {
            elem.removeAttribute('hidden')
        })
    }, {once: true})
})
