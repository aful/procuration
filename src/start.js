'use strict'

const config = require('./lib/config')
const logger = require('./lib/logging').getLogger()
const logger_console = require('./lib/logger_console')
const app = require('./app')

app.listen(config.server.port, config.server.ip, function() {
    logger_console.info('Server in', __dirname, 'started on', this.address())
    logger.info({app_root_dir_path: __dirname, app_address: this.address()})
})
