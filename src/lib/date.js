'use strict'

const moment = require('moment')

/**
 * Returns an Fr formatted date string from a Date
 *
 * @param {Date} date
 */
function toFrDate(date) {
    return date && moment(date).format('DD/MM/YYYY') || ''
}

/**
 * Returns an Fr formatted date string + time string from a Date
 *
 * @param {Date} date
 */
function toFrDateTime(date) {
    return date && moment(date).format('DD/MM/YYYY HH:mm') || ''
}

/**
 * Returns an ISO formatted date string (as expected by an SQL database)
 * from a Date.
 *
 * @param {Date} date
 */
function toIsoDateTime(date) {
    return date && moment(date).format('YYYY-MM-DD HH:mm:ss') || ''
}

/**
 * Returns a Date from an Fr formatted date string
 *
 * @param {string} date_fr
 * @returns {Date}
 */
function toDate(date_fr) {
    return moment(date_fr, 'DD/MM/YYYY')
}

module.exports = {
    toFrDate,
    toFrDateTime,
    toIsoDateTime,
    toDate
}
