'use strict'

const fs = require('fs')
const util = require('util')
const writeFile = util.promisify(fs.writeFile)
const readFile = util.promisify(fs.readFile)

const config = require('./config')
const logger = require('./logging').getLogger()

const WRITE_DELAY = 10000

let memory_storage
let write_timer_id

// const procurations = [
//     id: {
//         beneficiaire: {full_name},
//         donneurs: [{full_name, email}, {full_name, email}, {full_name, email}
//         ]
//     }
// ]
// const donneurs = {
//     id: {
//         full_name: <string>,
//         email: <email>,
//         person_type: <number>
//         ]
//     }
// }

/**
 * @returns {Function} a promise returning an Object
 */
function get(prop_name) {
    return Promise.resolve()
        .then(() => {
            if (!memory_storage) {
                logger.debug('memory_storage not loaded')
                return read()
            } else {
                logger.debug('memory_storage loaded')
            }
        })
        .then(() => {
            if (memory_storage[prop_name] == undefined) {
                memory_storage[prop_name] = {}
            }
            return memory_storage[prop_name]
        })
}

/**
 * @param {string} prop_name
 * @param {Object} prop_value
 * @returns {Function} a promise
 */
function set(prop_name, prop_value) {
    return Promise.resolve()
        .then(() => {
            if (!memory_storage) {
                return read()
            }
        })
        .then(() => {
            memory_storage[prop_name] = prop_value
        })
        .then(() => {
            scheduleWrite()
        })
        .then(() => {
            return memory_storage[prop_name]
        })
}

/**
 * This method deals with concurrent writes on the same file
 */
function scheduleWrite() {
    if (write_timer_id) {
        clearTimeout(write_timer_id)
    }

    write_timer_id = setTimeout(() => {
        write()
            .catch((err) => {
                logger.error(err)
            })
    }, WRITE_DELAY)
}

/**
 * @returns {Function} a promise
 */
function read() {
    return readFile(config.persistence.file_path, 'utf8')
        .then((data) => {
            memory_storage = JSON.parse(data)
        })
        .catch((err) => {
            if (err.code == 'ENOENT') {
                memory_storage = {}
            }
        })
}

/**
 * @returns {Function} a promise
 */
function write() {
    return writeFile(config.persistence.file_path, JSON.stringify(memory_storage))
}

process.on('SIGHUP', () => {
    logger.info('Got SIGHUP signal')

    if (write_timer_id) {
        clearTimeout(write_timer_id)
    }

    logger.info('Doing a final write')
    write()
        .then(() => {
            logger.info('Final write DONE')
            process.exit(0)
        })
})

module.exports = {
    get,
    set,
}
