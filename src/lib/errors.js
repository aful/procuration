/**
 * This module contains all the generic errors.
 */

// Errors only used in one module are not to be put into this module

'use strict'

const createError = require('create-error')

/**
 * Generic error when a user submits wrong values or when there are problems
 * about the processing of (valid) user submitted values.
 */
const SubmissionError = createError('SubmissionError')

module.exports = {
    SubmissionError,
}
