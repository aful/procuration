'use strict'

const path = require('path')
const bunyan = require('bunyan')

const config = require('./config')

let log_file_path = config.log.file_path
if (!path.isAbsolute(log_file_path)) {
    log_file_path = path.join(config.pkg_dir_path, log_file_path)
}
const logger = bunyan.createLogger({
    name: config.pkg.name,
    streams: [{
        level: config.log.level,
        stream: process.stdout,
    }]
})

/**
 * The logger to use to output application data as JSON to the filesystem
 */
function getLogger() {
    return logger
}

module.exports = {
    getLogger
}
