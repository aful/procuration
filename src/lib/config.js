'use strict'

const path = require('path')

const logger_console = require('./logger_console')

// We can't use the following because when running tests the main module is mocha.
// The filename of the main module (this is src/start.js)
//const main_module_file_path = require.main.filename;
//const src_dir_path = path.dirname(main_module_file_path);

// This is the directory path containing the package.json file
const pkg_dir_path = path.join(__dirname, '../..')
// The word "package" is reserved, thus we use "pkg"
const pkg = require(path.join(pkg_dir_path, 'package.json'))

const props = require('rc')(pkg.name, {
    pkg,
    pkg_dir_path,
    server: {
        port: 9091,
        ip: '0.0.0.0',
    },
    log: {
        file_path: `/tmp/${pkg.name}.log`,
        level: 'info',
    },
    persistence: {
        file_path: `/tmp/${pkg.name}.data.json`,
    },
    notification: false,
    domain: 'localhost',
    common: {
        secret: 'CHANGE_ME',
    },
})
logger_console.debug(`Using config file ${props.config}`)

module.exports = props
