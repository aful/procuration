'use strict'

const path = require('path')
const sprintf = require('sprintf-js').sprintf
const nodemailer = require('nodemailer')

const config = require('./config')
const logger = require('../lib/logging').getLogger()
const domain = config.urls.getMainDomainName()

const TEMPLATE_DIR_PATH = 'src/email_template'

const transport = nodemailer.createTransport({
    host: config.smtp.host,
    pool: true,
    port: 25 // Use plain SMTP, no TLS, no STARTTLS
})
const email_templates = {}

/**
 * @returns {Function} a promise
 */
function send(email_template_id, props) {
    if (!config.notification) {
        return Promise.resolve()
    }
    props.domain = domain
    let email_pieces = buildEmail(email_template_id, props)
    let email = {
        from: config.smtp.from,
        to: props.to,
        subject: email_pieces.subject,
        text: email_pieces.body
    }

    logger.debug('Sending email to:', props.to)
    return transport.sendMail(email)
}

/**
 * Builds an email pieces from the given template id and props
 *
 * @returns {Object} and object containing pieces of an email
 */
function buildEmail(email_template_id, props) {
    let template = getEmailTemplate(email_template_id)
    return {
        subject: sprintf(template.subject, props),
        body: sprintf(template.body, props)
    }
}

/**
 * Lazy-loads the requested email template
 *
 * @returns {Object} an email template object
 */
function getEmailTemplate(email_template_id) {
    if (email_templates[email_template_id]) {
        return email_templates[email_template_id]
    }

    let template_file_path = path.join(config.pkg_dir_path,
                                       TEMPLATE_DIR_PATH,
                                       email_template_id)
    // Legitimate use of non-global-require to have a generic code to load
    // templates.
    // eslint-disable-next-line global-require
    let template = require(template_file_path)
    email_templates[email_template_id] = template
    return template
}

module.exports = {
    send
}
