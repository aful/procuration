'use strict'

const app = require('../app')

const info = require('./info')
app.route('/info').get(info.getAppInfo)

const health = require('./health')
app.route('/health_check').get(health.check)

const procuration = require('./procuration')
app.route('/procurations')
    .post(procuration.create)
    .get(procuration.list)
app.route('/procurations/:procuration_id')
    .put(procuration.update)
