'use strict'

// const tracer = require('tracer').colorConsole()
const slugify = require('slugify')

const config = require('../lib/config')
const logger = require('../lib/logging').getLogger()
const persistence = require('../lib/persistence')
const SubmissionError = require('../lib/errors').SubmissionError

const PROCURATION_NATURAL_PERSON_MAX_COUNT = 3
const PROCURATION_LEGAL_PERSON_MAX_COUNT = 1
const PERSON_TYPE_NATURAL = 1
const PERSON_TYPE_LEGAL = 2

function create(req, res) {
    const beneficiaire_full_name = req.body.beneficiaire_full_name
    const donneur_full_name = req.body.donneur_full_name
    const donneur_email = req.body.donneur_email
    const donneur_person_type = Number(req.body.donneur_person_type)
    const procuration_id = slugify(beneficiaire_full_name, {lower: true})

    persistence.get('procurations')
        .then((procurations) => {
            if (procurations[procuration_id]) {
                throw new SubmissionError('Le bénéficiaire'
                                          + ` « ${beneficiaire_full_name} » est déjà `
                                          + 'listé, donnez votre procuration à'
                                          + ' ce niveau.')
            }

            procurations[procuration_id] = {
                beneficiaire: {full_name: beneficiaire_full_name},
                donneurs: {[donneur_email]: {
                    full_name: donneur_full_name,
                    email: donneur_email,
                    person_type: Number(donneur_person_type),
                }}
            }
            return persistence.set('procurations', procurations)
        })
        .then((procurations) => {
            req.flash('success', 'Votre procuration pour «'
                      + ` ${beneficiaire_full_name} » a bien été enregistrée.`)
            res.redirect('/procurations')
        })
        .catch((err) => {
            if (err instanceof SubmissionError) {
                req.flash('error', err.message)
                res.redirect('/procurations')
                return
            }

            logger.error(err)
            res.sendStatus(500)
        })
}

function list(req, res) {
    persistence.get('procurations')
        .then((procurations) => {
            // To ease if tests in nunjucks templates
            if (Object.keys(procurations).length == 0) {
                procurations = null
            }

            const data = {
                PROCURATION_NATURAL_PERSON_MAX_COUNT,
                PROCURATION_LEGAL_PERSON_MAX_COUNT,
                page_title: 'Procurations',
                procurations,
                messages: [],
                version: config.pkg.version,
            }
            let msg
            while ((msg = req.session.flash.pop())) {
                data.messages.push(msg)
            }

            res.render('procurations.njk', data)
        })
        .catch((err) => {
            logger.error(err)
            res.sendStatus(500)
        })
}

function update(req, res) {
    persistence.get('procurations')
        .then((procurations) => {
            const procuration_id = req.params.procuration_id
            const revoke_donneur_id = req.body.revoke_donneur_id
            const donneur_full_name = req.body.donneur_full_name
            const donneur_email = req.body.donneur_email
            const donneur_person_type = Number(req.body.donneur_person_type)
            const procuration = procurations[procuration_id]

            if (revoke_donneur_id) {
                delete procuration.donneurs[revoke_donneur_id]
                if (!Object.keys(procuration.donneurs).length) {
                    delete procurations[procuration_id]
                }
            }

            if (donneur_full_name && donneur_email) {
                const donneur_person_type_natural_count =
                          Object.entries(procuration.donneurs)
                          .filter(([id, val]) => {
                              return val.person_type == PERSON_TYPE_NATURAL
                          }).length
                const donneur_person_type_legal_count =
                          Object.entries(procuration.donneurs)
                          .filter(([id, val]) => {
                              return val.person_type == PERSON_TYPE_LEGAL
                          }).length

                if (donneur_person_type_natural_count >=
                    PROCURATION_NATURAL_PERSON_MAX_COUNT &&
                    donneur_person_type == PERSON_TYPE_NATURAL) {
                    throw new SubmissionError('Limite du nombre de procurations'
                                              + ' de personnes physiques atteinte')
                }

                if (donneur_person_type_legal_count >=
                    PROCURATION_LEGAL_PERSON_MAX_COUNT &&
                    donneur_person_type == PERSON_TYPE_LEGAL) {
                    throw new SubmissionError('Limite du nombre de procurations'
                                              + ' de personnes morales atteinte')
                }

                const donneur_id = slugify(donneur_email, {lower: true})
                if (!procuration.donneurs[donneur_id]) {
                    procuration.donneurs[donneur_id] = {
                        full_name: donneur_full_name,
                        email: donneur_email,
                        person_type: donneur_person_type,
                    }
                }
            }

            return persistence.set('procurations', procurations)
        })
        .then((procurations) => {
            res.redirect('/procurations')
        })
        .catch((err) => {
            if (err instanceof SubmissionError) {
                req.flash('error', err.message)
                res.redirect('/procurations')
                return
            }

            logger.error(err)
            res.sendStatus(500)
        })
}

module.exports = {
    create,
    list,
    update,
}
