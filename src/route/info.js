'use strict'

const config = require('../lib/config')

/**
 * Returns the app info either as JSON or as text depending on the query
 */
function getAppInfo(req, res) {
    // http://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Requested-With
    if (req.xhr) {
        res.json({
            name: config.pkg.name,
            version: config.pkg.version,
            full_name: config.pkg.name + '-' + config.pkg.version,
        })
        return
    }

    res.send(config.pkg.name + '-' + config.pkg.version + '\n')
}

exports.getAppInfo = getAppInfo
