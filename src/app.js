'use strict'

// const tracer = require('tracer').colorConsole()
const path = require('path')
const express = require('express')
const onFinished = require('on-finished')
const request_logger = require('morgan')
const helmet = require('helmet')
const methodOverride = require('method-override')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const nunjucks = require('nunjucks')

const config = require('./lib/config')
const logger = require('./lib/logging').getLogger()

// It's needed to access app from outside, such as for supertest tests
// otherwise one gets "Object #<Object> has no method 'address'" in tests
const app = module.exports = express()

// Securing the application, especially on HTTP headers.
// The Helmet middleware must be the first.
//
// This is the list of the helmet default middlewares minus the "ienoopen"
// middleware because this application only serves its own-produced HTML and
// resources and besides the "ienoopen" middleware prevents PDF and office files
// to be opened directly in IE.
app.use(helmet({
    ieNoOpen: false
}))

app.set('query parser', 'simple')

// Trust the specified IP addresses to provide true "X-Forwarded-*" headers.
// "X-Forwarded-*" headers are used to know the host name of the application as
// seen by the users. It's needed to compute the login URL on authentication
// redirects.
app.set('trust proxy', true)

// Log HTTP requests to the JSON logger
app.use((req, res, next) => {
    onFinished(res, () => {
        logger.info({
            method: req.method,
            url: req.url,
            status: res.statusCode
        })
    })
    next()
})
// And when not in PROD also print the HTTP requests in color to stdout
if (process.env.NODE_ENV != 'production') {
    app.use(request_logger('dev'))
}

// Static files
app.use('/procurations/bundle', express.static(path.join(__dirname, '/bundle')))
app.use('/procurations/img', express.static(path.join(__dirname, '/resource/img')))

app.use(cookieSession({
    name: config.pkg.name,
    // A secret is used to encrypt the session cookie
    secret: config.common.secret,
    domain: process.env.NODE_ENV == 'test' ? '127.0.0.1' : config.domain,
    // maxAge: expires at the end of session by default, later modified by
    // req.sessionOptions.maxAge for each logged user.
    //secureProxy: process.env.NODE_ENV != 'test',
    httpOnly: true
}))
// Flash middleware needs a session middleware, and the domain must be right if
// the session middleware relies on cookies.
app.use(require('flash')())

// Middleware to parse JSON input
app.use(bodyParser.json())
// Middleware to parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}))

// Method override to be able to use DELETE, PUT, etc. in HTML forms
// When using req.body, you must fully parse the request body
// before you call methodOverride() in your middleware stack,
// otherwise req.body will not be populated.
app.use(methodOverride((req, res) => {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // Look in urlencoded POST bodies and delete it
        /* eslint no-underscore-dangle: 0 */
        const method = req.body._method
        delete req.body._method
        return method
    }
}))

// Templating engine
nunjucks.configure(path.join(__dirname, 'view'), {
    autoescape: true,
    express: app,
})

require('./route')
