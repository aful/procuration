TODO
====

* Ajouter une case pré-cochée « Tout membre désigné par le CA » qui décochée
  rend les champs nominatifs disponibles

* Ne pas afficher le bouton « Donner procuration » pour les bénéficiaires ayant
  déjà reçu le nombre maximum de procurations

* Ajouter une notification par courriel pour notifier, dans le cas d'un ajout et
  dans le cas d'une suppression, à la fois le bénéficiaire et le donneur

* Contrôler qu'il n'y a pas de procuration croisées (X donne à Y qui donne à X)

* Dans le code renommer les noms :
  - procuration
  - beneficiaire
  - donneur

* Bloquer la possibilité de supprimer des procurations que l'on a pas faites
  soi-même (Comment fait Framadate ? Envoyer un courriel donnant un jeton
  d'autorisation à utiliser ?)

Ces fonctionnalités nécessitent une connexion à la base de données des membres
et ne seront donc pour l'instant pas ajouter :

* L'adresse email devrait être vérifiée par rapport à la base des membres (votre
  cotisation n'est pas à jour, vous n'avez plus de droit de vote)
