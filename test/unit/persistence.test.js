'use strict'

require('must')

const persistence = require('../../src/lib/persistence')

describe('Persistence', () => {

    it('has a working scheduleWrite', () => {
        return persistence.set('some_object1', {some_property : 'some_value'})
            .then(() => {
                return persistence.set('some_object2', {some_property : 'some_value'})
            })
            .then(() => {
                return persistence.set('some_object3', {some_property : 'some_value'})
            })
            .then(() => {
                return persistence.set('some_object4', {some_property : 'some_value'})
            })
            .then(() => {
                return persistence.set('some_object5', {
                    some_property1 : 'some_value1',
                    some_property2 : 'some_value2',
                    some_property3 : 'some_value3'
                })
            })
            .then(() => {
                return persistence.get('some_object1')
                    .then((obj) => {
                        obj.must.be.eql({some_property: 'some_value'})
                    })
            })
            .then(() => {
                return persistence.get('some_object5')
                    .then((obj) => {
                        obj.must.be.eql({
                            some_property1 : 'some_value1',
                            some_property2 : 'some_value2',
                            some_property3 : 'some_value3'
                        })
                    })
            })
    })

})
